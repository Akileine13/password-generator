$(function () {
    let chain = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789![]{}()%&*$#^@';

    // Adjust number
    $(document).on('input', '#formControlRange', function () {
        $('.range').html($(this).val());
    });

    // Generate password on click
    $('#button').on('click', function () {
        $('#password').val(password($('#formControlRange').val()));
        $('#hash').removeClass('invisible').text(sha256($('#password').val()));
    });

    // Select and copy on click in input
    $('#password').on('click', function () {
        $(this).select();
        document.execCommand('copy');
        $('.alert').removeClass('invisible').fadeIn(1000).fadeOut(2500);
    });
    // Function generate password
    function password(size) {
        let result = '';
        for (let i = 0; i < size; i++) {
            result += chain[Math.floor(Math.random() * chain.length)];
        }
        return result;
    }

});