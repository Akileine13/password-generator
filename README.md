# Password generator

*It's a small password generator of 8 to 32 characters in javascript.*

After cloning the project, launch:
```
npm install
```
If you haven't Node.js, you can download it [here](https://nodejs.org/en/).